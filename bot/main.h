#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

void resetArray(char* arr);
void loadSite(site* s, int id);
void *producer (void *q);
void *consumer (void *args);

/*id struct -> thread*/
typedef struct {
    int  id;
    queue* myqueue;
} id;

#endif // QUEUE_H_INCLUDED
