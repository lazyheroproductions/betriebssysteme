#include "queue.h"
#include "pthread.h"
#include "main.h"

#define NUM_CON 3
#define FOREVER while(1)

char steuerdatei[CHARSIZE];

int main(int argc, char *argv[]) {
    queue *fifo;
    pthread_t pro;
    pthread_t* cons;
    int i;
    id* idthread;

    if(argc == 2){
        // falls name der stuerdatei per kommandozeile
        // übertragen worden ist
        strcpy(steuerdatei, argv[1]);
    }else{
        /*Namen der Steuerdatei erfragen*/
        printf("Namen der Steuerdatei eingeben: \n");

        /*Angegebene Steuerdatei oeffnen*/
        if(fgets(steuerdatei, CHARSIZE, stdin) != NULL) {
            i = strlen(steuerdatei)-1;
            if( steuerdatei[ i ] == '\n'){
                    steuerdatei[i] = '\0';
            }else {
                printf("Das Lesen der Steuerdatei ist fehlgeschlagen!\n");
                exit(1);
            }
        }
    }

    /*Initialisierung der queue*/
    fifo = queueInit ();
    /*
    idthread=(id *)malloc (sizeof (id));
    idthread->myqueue=fifo;
    idthread->id=0; */
    /*Fehler: Initialiserung der Queue ist fehlgeschlagen*/
    if (fifo ==  NULL) {
        fprintf (stderr, "Error: Initialisierung der Warteschlange ist fehlgeschlagen.\n");
        exit(1);
    }else{
        printf("Initialisierung der Warteschlange abgeschlossen!\n");
    }

    /*Producer Thread erstellen*/
    pthread_create(&pro, NULL, producer, fifo);

    /*Consumer Threads erstellen*/
    int idcounter = 0;
    cons = (pthread_t *)malloc(sizeof(pthread_t) * QUEUESIZE);
    for(i=0; i<NUM_CON; i++) {
        idthread=(id *)malloc (sizeof (id));
        idthread->myqueue=fifo;
        idthread->id = idcounter;
        pthread_create(&cons[i], NULL, consumer, idthread);
        idcounter++;
    }

    /*Auf Consumer warten...*/
    for(i=0; i<NUM_CON; i++) {
        pthread_join(cons[i], NULL);
    }
    /*Auf Producer warten...*/
    pthread_join(pro, NULL);

    /*Aufräumen*/
    printf("Fertig! Queue wird gelöscht!");
    queueDelete (fifo);
    return 0;
}

 /*Zurücksetzten bzw. übeschreiben der Werte im bestehenden Array mit den übergebenen Werten*/
void resetArray(char* arr) {
    memset(arr, 0, CHARSIZE);
}

/*Download der Seite und Abspeicherung dieser in einer Datei*/
void loadSite(site* s, int id) {
    if(!s->domain) {
        // domain ist leer
        // nichts zu downloaden
        return;
    }
    /*Berechnung des dazugehoerigen Dateinamens*/
    char fileName[CHARSIZE] = {};
    strcat(fileName, "file_");
    char temp[CHARSIZE];

    /*Casten der 'id' zu einem String*/
    sprintf(temp, "%d", id);
    strcat(fileName, temp);
    strcat(fileName, "_");

    /*Array zurücksetzen/ueberschreiben*/
    resetArray(temp);

    /*Casten von 'i' zu einem String*/
    sprintf(temp, "%d", s->nr);
    strcat(fileName, temp);
    strcat(fileName, ".html");
    printf("%s\n", fileName);

    /*Herunterladen der Seite*/
    FILE* file;
    printf("Erstellen der Datei: %s...\n", fileName);
    file = fopen(fileName, "ab+");
    fclose(file);
    askServer(s->domain, s->route, fileName);
    printf("Download beendet!\n");
}

void *producer(void *q) {
    queue *fifo;
    fifo = (queue *)q;
    int numSites = 0;
    FILE* file;

    file = fopen(steuerdatei, "r");

    /*Error: Angegebene Datei konnte nicht geoeffnet werden*/
    if( file == NULL ) {
        printf("Error: Die angegebene Datei konnte nicht gelesen werden!\n");
        exit(-1);
    /*Öffnen erfolgreich*/
    }else{
        printf("Die angegebene Datei konnte erfolgreich geoeffnet werden!\n");
    }

    /*Lesen der Datei*/
    char ch;
    site* s = malloc(sizeof *s);
    int d = 0;  /*Zähler für die 'domain'*/
    int r = 0;  /*Zähler für den 'router*/
    /*Anfangseinstellung: mode 'd': char zur 'domain' hinzufügen*/
    char mode = 'd';

    /*Handhaben von Leerzeichen und Umbrüchen*/
    while( (ch = fgetc(file)) != EOF ) {
        if(ch == ' ') {
            /*Setzen des Modus auf 'r'*/
            /*mode 'r': char zur 'route' hinzufügen*/
            mode = 'r';
        }
        if(ch == '\n') {
            /*Setzen des Modus auf 'a'*/
            /*mode 'a': Seite hinzufügen*/
            mode = 'a';
        }

        switch(mode) {
        /*Case 'a': Erstellen einer neuen Seite mit den 'domain' und 'route' strings*/
        case 'a':
            /*Warten auf die Warteschlange*/
            pthread_mutex_lock(fifo->mut);
            while (fifo->full) {
                pthread_cond_wait(fifo->notFull, fifo->mut);
            }
            pthread_mutex_unlock(fifo->mut);
            /*Hinzufuegen einer neuen Seite zur Warteschlange*/
            site* temp = (site* ) malloc(sizeof(site));
            strcpy(temp->domain, s->domain);
            strcpy(temp->route, s->route);
            temp->nr = ++numSites;
            pthread_mutex_lock(fifo->mut);
            queueAdd(q, &temp);
            pthread_cond_signal(fifo->notEmpty);
            pthread_mutex_unlock(fifo->mut);
            printf("Datei wurde hinzugefuegt!\n");

            /*Zurücksetzten/Überschreiben der Werte im Array*/
            resetArray(s->domain);
            resetArray(s->route);
            d = 0;
            r = 0;
            mode = 'd';
            break;

        case 'd':
            /*Case 'd': Übergebenen char zur 'domain' hinzufügen*/
            s->domain[d] = ch;
            d++;
            break;

        case 'r':
            /*Case 'r': Übergebenen char zur 'route' hinzufügen*/
            /*Auftreten von Leerzeichen handhaben*/
            if(ch == ' '){
                break;
            }
            s->route[r] = ch;
            r++;
            break;

        default:
            /*Sollte niemals auftreten*/
            printf("Fatal Error: Beim Lesen der Datei ist ein Fehler aufgetreten!\n");
            break;
        }
    }
    fclose(file);
    printf("Lesen der Datei wurde beendet!\n");
    // fifo->end = 1;
    pthread_mutex_lock(fifo->mut);
    fifo->end = 1;
    pthread_cond_signal(fifo->notEmpty);
    pthread_mutex_unlock(fifo->mut);

    return (NULL);
}

void *consumer(void *q) {
    queue *fifo;
    id *fifo2;
    fifo2 = (id *)q;
    site* download;// = fifo2->myqueue->buf[fifo2->id];
    fifo = fifo2->myqueue;
    int this_id = fifo2->id;

    FOREVER{
        /*lock mutex */
        pthread_mutex_lock (fifo->mut);
        if(fifo->empty == 1) {
            if(fifo->end) {
                /*unlock mutex */
                pthread_mutex_unlock (fifo->mut);
                pthread_exit(0);
                /*wir gehen aus der Schleife raus*/
                return (NULL);
            }else{/* Warteschlange leer*/
                /* Warten bis Warteschalnge nicht mehr leer*/
                pthread_cond_wait (fifo->notEmpty, fifo->mut);
                pthread_mutex_unlock (fifo->mut);
            }
        } else {
            /*Herausnehmen*/
            queueDel(fifo, &download);
            pthread_cond_signal(fifo->notFull);
            pthread_mutex_unlock (fifo->mut);
            loadSite(download, this_id);
        }
    }
    return (NULL);
}
