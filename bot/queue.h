#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "msocket.h"

#define QUEUESIZE 5
#define CHARSIZE 200

/*site struct*/
typedef struct {
    int  nr;
    char domain[CHARSIZE];
    char route[CHARSIZE];
} site;

/*queue struct*/
typedef struct {
    site** buf;
    int head, tail;
    int full, empty, end;
    pthread_mutex_t *mut;
    pthread_cond_t *notFull, *notEmpty; /*Conditions*/
} queue;

queue *queueInit (void);
void queueDelete (queue *q);
void queueAdd (queue *q, site** s);
int queueDel (queue *q, site** out);

#endif // QUEUE_H_INCLUDED
