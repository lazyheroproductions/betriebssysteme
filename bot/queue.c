#include "queue.h"

queue* queueInit(void) {
    queue *q;

    site** sites; // array von site
    sites = (site** ) malloc( (sizeof(site) * QUEUESIZE));
    q = (queue *)malloc (sizeof (queue));

    if (q == NULL){
        return (NULL);
    }

    q->empty = 1;
    q->full = 0;
    q->end = 0;
    q->buf = sites;
    q->head = 0;
    q->tail = 0;
    q->mut = (pthread_mutex_t *) malloc (sizeof (pthread_mutex_t));
    pthread_mutex_init (q->mut, NULL);
    q->notFull = (pthread_cond_t *) malloc (sizeof (pthread_cond_t));
    pthread_cond_init (q->notFull, NULL);
    q->notEmpty = (pthread_cond_t *) malloc (sizeof (pthread_cond_t));
    pthread_cond_init (q->notEmpty, NULL);
    return (q);
}

void queueAdd(queue *q, site** s) {
    printf("Kopieren der Werte von Seite: %s \n", (*s)->domain);
    q->buf[q->tail] = *s;
    q->tail++;

    if (q->tail >= QUEUESIZE){
        q->tail = 0;
    }
    if (q->tail == q->head) {
        q->full = 1;
        printf("Warteschlange ist voll!\n");
    }
    q->empty = 0;
    return;
}

void queueDelete(queue *q) {
    pthread_mutex_destroy (q->mut);
    free (q->mut);
    pthread_cond_destroy (q->notFull);
    free (q->notFull);
    pthread_cond_destroy (q->notEmpty);
    free (q->notEmpty);
    free (q);
}

int queueDel(queue *q, site** out) {
    /*Error: Nullpointer*/
    if(q == NULL || out == NULL) {
        printf("Error: Nullpointer uebergeben!");
        return 0;
    } else {
        /*Seite aus der Queue lesen und in out speichern*/
        *out = q->buf[q->head];
        printf("Entfernen der Seite: %s \n", (*out)->domain);
        q->head++;
        if (q->head >= QUEUESIZE){
            q->head = 0;
        }
        if (q->head == q->tail){
            q->empty = 1;
        }
        q->full = 0;
        return 1;
    }
}
