#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

void auswerten (char* argumente[], char* verzeichnis, int abfrageA, int abfrageL);
void ausgabeInformationen(char* argumente[], struct dirent* dirEntry, char* verzeichnis);

int main(int argumentenanzahl, char* argumente[]){

    int abfrageA=0;
    int abfrageL= 0;
    char ergebnisGetopt;

    char* verzeichnis;

    while ((ergebnisGetopt = getopt (argumentenanzahl, argumente, "al")) != -1){
        /*Parameterabfrage*/
        switch (ergebnisGetopt){
            case 'a':
                abfrageA= 1;
                break;
            case 'l':
                abfrageL= 1;
                break;
            /*Fehlerfall*/
            case '?':
                printf("Parameter nicht erkannt.\n");
                exit(1);
        }
    }
    /*gleiches Verzeichnis*/
    if (argumentenanzahl <= optind){
        verzeichnis = ".";
    }
    /*anderes Verezeichnis*/
    else{
        verzeichnis = argumente[optind];
    }

    auswerten(argumente, verzeichnis,abfrageA,abfrageL);

    return 0;
}

void auswerten (char* argumente[], char* verzeichnis, int abfrageA, int abfrageL){

    DIR* currDir;
    struct dirent* currFile;

    if (chdir(verzeichnis)!=-1){
        /*Verzeichnis öffnen*/
        currDir = opendir(".");

        /*Je nach Bedingung, lesen des Verzeichnisses*/
        if (currDir!=NULL){
            /**ls**/
            if(!abfrageA && !abfrageL){
                while((currFile= readdir(currDir))!=0){
                    /*Ausgabe der enthaltenen Dateien des Verzeichnisses (ohne '.')*/
                    if(currFile->d_name[0]!='.'){
                        printf("%s\n", currFile->d_name);
                    }
                }
            }
            /**ls -a**/
            if(abfrageA && !abfrageL){
                while((currFile= readdir(currDir))!=0){
                    /*Ausgabe der enthaltenen Dateien des Verzeichnisses (mit '.')*/
                    printf("%s\n", currFile->d_name);
                }
            }
            /**ls -l**/
            if(!abfrageA && abfrageL){
                while((currFile= readdir(currDir))!=0){
                    /*Ausgabe aller Informationen (ohne '.')*/
                    if(currFile->d_name[0]!='.'){
                        ausgabeInformationen(argumente, currFile,verzeichnis);
                    }
                }
            }
            /**ls -al**/
            if(abfrageA && abfrageL){
                while((currFile= readdir(currDir))!=0){
                    /*Ausgabe aller Informationen (mit '.')*/
                    ausgabeInformationen(argumente,currFile,verzeichnis);
                }
            }
        }
        closedir(currDir);

    }else{
        printf("Verzeichnis konnte nicht geöffnet werden\n");
    }
}

void ausgabeInformationen(char* argumente[], struct dirent* dirEntry, char* verzeichnis){
    /*Untersuchung der Dateieigenschaften*/
    struct stat fileStat; /*Metainformationen der Datei*/
    lstat(dirEntry->d_name, &fileStat);

    /*Zugriffrechte*/
    printf( (S_ISDIR(fileStat.st_mode))  ? "d" : "-"); /*Testmakro: Ist 'fileStat.st_mode' ein Verzeichnis?*/
    printf( (fileStat.st_mode & S_IRUSR) ? "r" : "-"); /* Zugriffsrecht Benutzer: Read*/
    printf( (fileStat.st_mode & S_IWUSR) ? "w" : "-"); /* Zugriffsrecht Benutzer: Write*/
    printf( (fileStat.st_mode & S_IXUSR) ? "x" : "-"); /* Zugriffsrecht Benutzer: Execute*/
    printf( (fileStat.st_mode & S_IRGRP) ? "r" : "-"); /* Zugriffsrecht Gruppe: Read*/
    printf( (fileStat.st_mode & S_IWGRP) ? "w" : "-"); /* Zugriffsrecht Gruppe: Write*/
    printf( (fileStat.st_mode & S_IXGRP) ? "x" : "-"); /* Zugriffsrecht Gruppe: Execute*/
    printf( (fileStat.st_mode & S_IROTH) ? "r" : "-"); /* Zugriffrecht andere: Read*/
    printf( (fileStat.st_mode & S_IWOTH) ? "w" : "-"); /* Zugriffrecht andere: Write*/
    printf( (fileStat.st_mode & S_IXOTH) ? "x" : "-"); /* Zugriffrecht andere: Execute*/
    printf(" ");

    /* Zahl der Links auf diese Datei */
    printf("%3d ", fileStat.st_nlink);

    /* User ID des Dateibesitzers */
    struct passwd* pass;
    pass = getpwuid(fileStat.st_uid);
    printf("%s ", pass->pw_name);

    /* Group ID des Dateibesitzers */
    struct group* g;
    g = getgrgid(fileStat.st_gid);
    printf("%s ", g->gr_name);

    /* Dateigroesse in bytes */
    printf("%7d ", fileStat.st_size);

    /* Zeit der letzten Modifikation */
    char buff[20];
    struct tm* timeinfo;
    timeinfo = localtime (&fileStat.st_mtim.tv_sec);
    strftime(buff, sizeof(buff), "%b %d %H:%M ", timeinfo);
    printf("%s", buff);

    /*Name*/
    printf(dirEntry->d_name);
    printf("\n");
}
