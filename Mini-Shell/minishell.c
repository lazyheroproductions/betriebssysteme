#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>

/*
 * Defines
 */
#define INPUTLINEBUFFER_LENGTH 128
#define RET_EXIT -1
#define PATH_BUFFERLENGTH 512

/*
 * Globals
 */
char *user;
char *cwd; //current working directory

int main(int argc, char** argv) {

    printf("To exit press Ctrl+C :)\n");
    //user = getenv("USER");
    while(1){
        cmdPrompt();
    }
    return (EXIT_SUCCESS);
}

int cmdPrompt(){
    char cwd[INPUTLINEBUFFER_LENGTH];
    char input[INPUTLINEBUFFER_LENGTH];
    //free(cwd);
    getcwd(cwd, INPUTLINEBUFFER_LENGTH); // get working directory
    printf("%s: -> %s>", getlogin() , cwd);
    fgets(input, INPUTLINEBUFFER_LENGTH, stdin);
    /* remove newline just in case */
    if (input[strlen(input)-1] == '\n'){
	input[strlen(input)-1] = '\0';
    }

    if (strcmp(input, "exit") == 0)
        // input was "shell"
        exit(1);
    else if (strncmp(input, "cd", 2) == 0) {
        // input was cd -> we're changing the directory
        return changeDirectory(input);
    }
    else {
        // everything else
        // create new process
        return executeCmd(input);
    }
}

// is not in use
int changeDirectory(char* input){
    char *cd_dir;
    char *tmp_ptr;

    /* check input and cut "cd" command */
    if (!strtok(input, " ") || !(cd_dir = strtok(NULL, " "))) {
            return -1;
    }
    if(strcmp(cd_dir, "~") == 0){
        chdir(getenv("HOME"));
    }else{
        chdir(cd_dir);
    }
}

char* splitString(char* cmd, const char splitter){
   char* token;
    /* get the first token */
   token = strtok(cmd, splitter);

   /* walk through other tokens */
   while(token != NULL)
   {
      token = strtok(NULL, splitter);
   }

   return token;
}

int executeCmd(char* input){
    // Tokenize input
    char* args[16];
    char* ptr;
    int pid;
    int i = 0;
    ptr = strtok(input, " ");
    // Save first token
    args[0] = ptr;
    // Go to next token until the end
    while((ptr = strtok(NULL, " ")) != NULL) {
        i++;
        args[i] = ptr;
    }
    // add NULL pointer for execvp
    i++;
    args[i] = (char*) NULL;

    // Check if we are in a child process
    if( (pid=fork()) < 0) {
        // Fork not successful
        printf("Fork fehlgeschlagen!");
    }
    else if(pid == 0) {
        // Process is child process
        // Execute command(s) in args
        if(execvp(args[0], args) < 0) {
            printf("Couldn't execute child command!\Try again\n");
        }
        // Success!
        exit(1);
    }
    else {
        // Process is parent process
        waitpid(pid,NULL, 0);
    }
    return 1;
}
